/**
 * \file Personne.cpp
 * \brief On implémente les différentes fonctions et méthodes définies dans les fichiers d'en-tête. La compilation de ce fichiers .cpp nous permettra d'utiliser dans le main ces fonctions
Il inclut les définitions à partir d'un en-tête.
 * \author Matis MAZINGUE Joan PIERRON Maxence LANONE Hugo PRADIER G7
 * \date 31 Mars 2022
 */

#include <iostream>
#include "Personne.hpp"

using namespace std;
using namespace reseau;

/**
 * @brief Construct a new Personne:: Personne object
 * 
 * @param prenom 
 */
Personne::Personne(const std::string& prenom):prenom{prenom}{
	// cout << "Personne créée " << prenom <<"\n";
}

// Personne::Personne(): numPers{0}{
// 	cout << "Personne créée \n";
// }

/**
 * @brief 
 * 
 * @return const string& 
 */
const string& Personne::getPrenom() const{
	return this->prenom;
}

