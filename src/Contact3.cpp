/**
 * \file Contact3.cpp
 * \brief On implémente les différentes fonctions et méthodes définies dans les fichiers d'en-tête. La compilation de ce fichiers .cpp nous permettra d'utiliser dans le main ces fonctions
Il inclut les définitions à partir d'un en-tête.
 * \author Matis MAZINGUE Joan PIERRON Maxence LANONE Hugo PRADIER G7
 * \date 31 Mars 2022
 */

#include <iostream>
#include "Contact3.hpp"

using namespace std;
using namespace reseau2;

/**
 * @brief 
 * 
 * @return list<const Personne*> 
 */
list<const Personne*> ListeContact::getValue() {
			return personnes;
}

/**
 * @brief 
 * 
 * @param p 
 */
void ListeContact::ajtContact(const Personne& p) {
			personnes.push_back(&p);
}

/**
 * @brief Construct a new Liaison Personne:: Liaison Personne object
 * 
 * @param envoyeur 
 * @param destinataire 
 */
LiaisonPersonne::LiaisonPersonne(const Personne *envoyeur, const Personne *destinataire) : envoyeur(envoyeur), destinataire(destinataire) {}

/**
 * @brief 
 * 
 * @return const Personne* 
 */
const Personne* LiaisonPersonne::getEnvoyeur() const {
	return envoyeur;
}

/**
 * @brief 
 * 
 * @return const Personne* 
 */
const Personne* LiaisonPersonne::getDestinataire() const {
	return destinataire;
}

/**
 * @brief Construct a new Conteneur:: Conteneur object
 * 
 * @param pers 
 */
Conteneur::Conteneur(Personne *pers) : pers(pers) {}

/**
 * @brief 
 * 
 * @return std::list<const Personne*> 
 */
std::list<const Personne*> Conteneur::getValue() {
	cout << "test";
	std::list<const Personne*> liste;
	for (const LiaisonPersonne& liaisonPersonne : personnes) {
		liste.push_back(liaisonPersonne.getDestinataire());
	}
	return liste;
}

/**
 * @brief 
 * 
 * @param p 
 */
void Conteneur::ajtContact(const Personne& p) {
	personnes.emplace_back(pers, &p);
}