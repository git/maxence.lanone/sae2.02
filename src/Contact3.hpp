/**
 * \file Contact3.hpp
 * \brief Contient des variables, des constantes et divers types de données relatifs à notre classe Contact. Cela nous permet de stocker des composants de code.
 * \author Matis MAZINGUE Joan PIERRON Maxence LANONE Hugo PRADIER G7
 * \date 31 Mars 2022
 */

#ifndef CONTACT3_hpp
#define CONTACT3_hpp
#include<list>
#include<iostream>
#include<set>

#include "Personne3.hpp"

namespace reseau2{
	class Contact {
	public:
		virtual std::list<const Personne*> getValue() = 0;
		virtual void ajtContact(const Personne& p) = 0;
		virtual ~Contact() = default;
	};

	class ListeContact : public Contact {
		std::list<const Personne*> personnes;
	public:
		std::list<const Personne*> getValue() override;
		void ajtContact(const Personne& p) override;
	};

	class LiaisonPersonne {
		const Personne* envoyeur;
		const Personne* destinataire;
	public:
		LiaisonPersonne(const Personne *envoyeur, const Personne *destinataire);
		const Personne *getEnvoyeur() const;
		const Personne *getDestinataire() const;
	};

	class Conteneur : public Contact {
		Personne* pers;
		std::list<LiaisonPersonne> personnes;
	public:
		Conteneur(Personne *pers);
		std::list<const Personne*> getValue() override;
		void ajtContact(const Personne& p) override;
	};
}

#endif
