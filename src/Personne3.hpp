/**
 * \file Personne3.hpp
 * \brief Contient des variables, des constantes et divers types de données relatifs à notre classe Personne. Cela nous permet de stocker des composants de code.
 * \author Matis MAZINGUE Joan PIERRON Maxence LANONE Hugo PRADIER G7
 * \date 31 Mars 2022
 */

#ifndef PERSONNE3_hpp
#define PERSONNE3_hpp

#include <string>

namespace reseau2 {
	class Contact;
	class Personne {
		std::string prenom;
		Contact* contacts;
	public:
		explicit Personne(const std::string& prenom);
		void ajtContact(const Personne& personne);
		const std::string& getPrenom() const;
		bool peutCommuniquerA(const Personne& p) const;
		~Personne();
	};
}

#endif

