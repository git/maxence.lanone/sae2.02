/**
 * \file Personne.hpp
 * \brief Contient des variables, des constantes et divers types de données relatifs à notre classe Personne. Cela nous permet de stocker des composants de code.
 * \author Matis MAZINGUE Joan PIERRON Maxence LANONE Hugo PRADIER G7
 * \date 31 Mars 2022
 */

#ifndef PERSONNE_hpp
#define PERSONNE_hpp

namespace reseau {
	class Personne {
		std::string prenom;
	public:
		Personne(const std::string& prenom);
		// Personne();
		const std::string& getPrenom() const;
		// ~Personne();
	};
}

#endif

