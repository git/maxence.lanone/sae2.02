/**
 * \file main.cpp
 * \brief Fonctions permettant d'appeller des fonctions à l'aide d'un menu et de mettre en place un réseau
 * \author Matis MAZINGUE Joan PIERRON Maxence LANONE Hugo PRADIER G7
 * \date 31 Mars 2022
 */




#include <iostream>
#include "Personne3.hpp"
#include "Contact3.hpp"
#include "Personne.hpp"
#include "Contact.hpp"
#include <vector>

using namespace std;
using namespace reseau;

int main() {
	/** 
	* \brief Main divisé en deux partie :
	*
	*	Partie 1 : qui qui permet la création de personnes appartent au futur réseau, la création à partir de ces personnes de propriétaire possédant *	une liste de contact, enfin on ajoute les contacts directs dans la liste de chaque propriétaire.
	*
	*	Partie 2 : menu qui permet à l'utilisateur d'appeller des fonctions pour réaliser les différentes techniques, il va pouvoir pour chacune *	afficher la liste de contact direct et indirect pour une ou toutes les personnes.
	*
	*	choix, choix2, choix3, persChoix : correspond à la demande de l'utlisateur qui entraîne l'appel de la fonction souhaitée
	*/

	/* 
	On crée chaque personne qui seront dans notre réseau 
	*/

	reseau::Personne p1{"Bob"};
	reseau::Personne p2{"Domi"};
	reseau::Personne p3{"Anne"};
	reseau::Personne p4{"Elie"};
	reseau::Personne p5{"Cali"};
	reseau::Personne p6{"Samuel"};
	reseau::Personne p7{"Elena"};
	reseau::Personne p8{"Baptiste"};
	reseau::Personne p9{"Franck"};
	reseau::Personne p10{"Agathe"};
	reseau::Personne p11{"Louna"};
	reseau::Personne p12{"Theo"};
	reseau::Personne p13{"Morgane"};
	reseau::Personne p14{"Thibault"};
	reseau::Personne p15{"Gwen"};
	list<Contact*> Contacts; 
	/*
	On crée les contacts en mettant leur propiétaire, on ajoute ensuite chaque contact dans une liste juste après leur construction
	*/
	Contact c1{p1};
	Contacts.push_back(&c1);
	Contact c2{p2};
	Contacts.push_back(&c2);
	Contact c3{p3};
	Contacts.push_back(&c3);
	Contact c4{p4};
	Contacts.push_back(&c4);
	Contact c5{p5};
	Contacts.push_back(&c5);
	Contact c6{p6};
	Contacts.push_back(&c6);
	Contact c7{p7};
	Contacts.push_back(&c7);
	Contact c8{p8};
	Contacts.push_back(&c8);
	Contact c9{p9};
	Contacts.push_back(&c9);
	Contact c10{p10};
	Contacts.push_back(&c10);
	Contact c11{p11};
	Contacts.push_back(&c11);
	Contact c12{p12};
	Contacts.push_back(&c12);
	Contact c13{p13};
	Contacts.push_back(&c13);
	Contact c14{p14};
	Contacts.push_back(&c14);
	Contact c15{p15};
	Contacts.push_back(&c15);


	/*
	On place les contacts directs dans une liste de personne qui correspond au répertoire de chaque personne
	*/
	c1.ajouterContact(p2);
	c1.ajouterContact(p5);
	c1.ajouterContact(p4);
	c3.ajouterContact(p1);
	c3.ajouterContact(p2);
	c4.ajouterContact(p5);
	c4.ajouterContact(p6);
	c5.ajouterContact(p4);
	c6.ajouterContact(p7);
	c6.ajouterContact(p9);
	c7.ajouterContact(p3);
	c8.ajouterContact(p2);
	c9.ajouterContact(p6);
	c9.ajouterContact(p11);
	c10.ajouterContact(p7);
	c10.ajouterContact(p8);
	c11.ajouterContact(p15);
	c12.ajouterContact(p10);
	c13.ajouterContact(p11);
	c14.ajouterContact(p12);
	c14.ajouterContact(p13);
	c15.ajouterContact(p9);

	// ----------------------------------------------- 

	reseau2::Personne pp1{"Bob"};
	reseau2::Personne pp2{"Domi"};
	reseau2::Personne pp3{"Anne"};
	reseau2::Personne pp4{"Elie"};
	reseau2::Personne pp5{"Cali"};
	reseau2::Personne pp6{"Samuel"};
	reseau2::Personne pp7{"Elena"};
	reseau2::Personne pp8{"Baptiste"};
	reseau2::Personne pp9{"Franck"};
	reseau2::Personne pp10{"Agathe"};
	reseau2::Personne pp11{"Louna"};
	reseau2::Personne pp12{"Theo"};
	reseau2::Personne pp13{"Morgane"};
	reseau2::Personne pp14{"Thibault"};
	reseau2::Personne pp15{"Gwen"};
	pp1.ajtContact(pp2);
	pp1.ajtContact(pp5);
	pp1.ajtContact(pp4);
	pp3.ajtContact(pp1);
	pp3.ajtContact(pp2);
	pp4.ajtContact(pp5);
	pp4.ajtContact(pp6);
	pp5.ajtContact(pp4);
	pp6.ajtContact(pp7);
	pp6.ajtContact(pp9);
	pp7.ajtContact(pp3);
	pp8.ajtContact(pp2);
	pp9.ajtContact(pp6);
	pp9.ajtContact(pp11);
	pp10.ajtContact(pp7);
	pp10.ajtContact(pp8);
	pp11.ajtContact(pp15);
	pp12.ajtContact(pp10);
	pp13.ajtContact(pp11);
	pp14.ajtContact(pp12);
	pp14.ajtContact(pp13);
	pp15.ajtContact(pp9);


	vector<reseau::Contact> Repertoire {c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15};
	vector<reseau2::Personne> Repertoire2 {pp1, pp2, pp3, pp4, pp5, pp6, pp7, pp8, pp9, pp10, pp11, pp12, pp13, pp14, pp15};

	/*
	Début du menu et initialisation des variables qu'on utilisera pour chaque partie de celui-ci 
	*/
	int choix, choix2, choix3, persChoix, persDest, persEnvoie;


	cout <<"\n  ---------------------------------------------------------------\n  |  Technique n°1  (1)  |  Technique n°3  (2)  |  Sortir  (3)  |\n  ---------------------------------------------------------------\n\n--> ";
	cin >> choix;
	switch(choix) {
		case 1 :

			cout << "\n  -------------------------------------------------------------\n  |  Cas global  (1)  |  Cas individuel  (2)  |  Retour  (3)  |\n  -------------------------------------------------------------\n\n--> "; 
			cin >> choix2;
			switch(choix2) {
					/*
					Le cas 1 et le cas 2 feront le même code au début donc on appliquera leurs différences plus tard dans un if
					*/
				case 1:
				case 2:

					cout <<"\n  -------------------------------------------------------------------\n  |  Contact Direct  (1)  |  Contact Indirect  (2)  |  Retour  (3)  |\n  -------------------------------------------------------------------\n\n--> "; 
					cin >> choix3;
					switch(choix3) {
						/*
						Le cas 1 et le cas 2 feront le même code au début donc on appliquera leurs différences plus tard dans un autre switch
						*/
						case 1:
						case 2:

							/*
							On applique en fonction de notre choix2 les méthodes et l'affichage voulu, afin d'éviter des répétitions dans les case.
							*/
							if(choix2==2){

								cout << "\n Choisissez la personne que vous voulez regarder : \n\n--------------------\n(1)\tBob\n(2)\tDomi\n(3)\tAnne\n(4)\tElie\n(5)\tCali\n(6)\tSamuel\n(7)\tElena\n(8)\tBaptiste\n(9)\tFranck\n(10)\tAgathe\n(11)\tLouna\n(12)\tTheo\n(13)\tMorgane\n(14)\tThibault\n(15)\tGwen\n--------------------\n\n Tapez le numéro de la personne : ";
								cin >> persChoix ;
								cout << "\n";
								/*
								On applique en fonction de notre choix3 les méthodes voulu, afin d'éviter des répétitions dans les case.
								*/
								switch(choix3) {
									case 1:
									/*
									En fonction de la personne choisi avec la variable persChoix, il affiche ses contacts directs  
									*/
										Repertoire[persChoix-1].afficherContactDirect();
										break;
									case 2:
									/*
									En fonction de la personne choisi avec la variable persChoix, il affiche sa propagation  
									*/
										Repertoire[persChoix-1].afficherContactIndirect(Contacts);
										break;
                                    default:
                                        cout<<"Erreur : Tapez un nombre correct\n";
                                        exit(0);
								}
							}
							else{
								switch(choix3){
									case 1:
										/*
										On affiche les contacts directs de tout le réseau
										*/
										for (long unsigned int i=0; i<Repertoire.size(); i++){
											Repertoire[i].afficherContactDirect();
										}										 
										break;
									case 2:
										/*
										On affiche la propagation de tout le réseau
										*/
										for (long unsigned int i=0; i<Repertoire.size(); i++){
											Repertoire[i].afficherContactIndirect(Contacts);
										}	
										break;
                                    default:
                                        cout<<"Erreur : Tapez un nombre correct\n";
                                        exit(0);
								}
							}
							break;

						case 3:
							/*
							les deux prochains case sont là pour appeler récursivement le main et donc notre menu
							*/
							main();
							break;
                        default:
                            cout<<"Erreur : Tapez un nombre correct\n";
                            exit(0);
					}
					break;
				case 3:
					main();
					break;
                default:
                    cout<<"Erreur : Tapez un nombre correct\n";
                    exit(0);
			}
			/*
			On attend 8 secondes après l'affichage de nos commandes puis on effache tout, pour avoir un ternimal vide entre chaque utilisation
			*/
			main();
			break;
		case 2 :
			cout << "\n Choisissez la personne qui envoie : \n\n--------------------\n(1)\tBob\n(2)\tDomi\n(3)\tAnne\n(4)\tElie\n(5)\tCali\n(6)\tSamuel\n(7)\tElena\n(8)\tBaptiste\n(9)\tFranck\n(10)\tAgathe\n(11)\tLouna\n(12)\tTheo\n(13)\tMorgane\n(14)\tThibault\n(15)\tGwen\n--------------------\n\n Tapez le numéro de la personne : ";
			cin >> persEnvoie;
			cout << "\n";
			cout << "\n Choisissez le destinataire : \n\n--------------------\n(1)\tBob\n(2)\tDomi\n(3)\tAnne\n(4)\tElie\n(5)\tCali\n(6)\tSamuel\n(7)\tElena\n(8)\tBaptiste\n(9)\tFranck\n(10)\tAgathe\n(11)\tLouna\n(12)\tTheo\n(13)\tMorgane\n(14)\tThibault\n(15)\tGwen\n--------------------\n\n Tapez le numéro de la personne : ";
            cin >> persDest;
            cout<<"test";
			cout << "\n";
            //Personne f= Repertoire2[persEnvoie-1]

            Repertoire2[persEnvoie-1].peutCommuniquerA(Repertoire2[persDest-1]);
			main();
			break;
		case 3 :
			/* le return sert pour arrêter le programme lorsqu'on demande l'exit sur le premier menu*/
			exit(0); /*  */
			break;
        default:
            cout<<"Erreur, tapez un chiffre correct\n";
            exit(0);

		}


}

