/**
 * \file Contact.hpp
 * \brief Contient des variables, des constantes et divers types de données relatifs à notre classe Contact. Cela nous permet de stocker des composants de code.
 * \author Matis MAZINGUE Joan PIERRON Maxence LANONE Hugo PRADIER G7
 * \date 31 Mars 2022
 */

#ifndef CONTACT_hpp
#define CONTACT_hpp
#include"Personne.hpp"
#include<list>
#include<iostream>
#include<set>


namespace reseau{
	class Contact {
		reseau::Personne Proprietaire;
		std::list<const reseau::Personne*> lesContacts{};
		void Recursive(std::list<Contact*> Contacts, std::set<const reseau::Personne*>* set);
	public:

		Contact(const reseau::Personne& Proprietaire);
		const Personne& getProprio() const;
		bool ajouterContact(const reseau::Personne& unContact);
		bool supprimerContact(const reseau::Personne& unContact);
		bool rechercher(const Personne& unContact);
		void afficherContactDirect();
		void afficherContactIndirect(std::list<Contact*> Contacts);
		// ~Contact();
	};
	std::ostream& operator<<(std::ostream& os, reseau::Personne p);
	bool operator==(reseau::Personne p1, reseau::Personne p2);
}

#endif
